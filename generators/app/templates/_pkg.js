/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * <%= description %>
 * @module <%= name %>
 * @author 
 * @since 0.1.0
 <% if ( events ) { %>* @requires <%= name %>/events <% } %>
 <% if ( commands ) { %>* @requires <%= name %>/commands <% } %>
 <% if ( models ) { %>* @requires <%= name %>/models <% } %>
 <% if ( lib ) { %>* @requires <%= name %>/lib <% } %>
 */

<% if ( events ) { %>
module.exports = require('./events');
	<% if ( lib ) { %>
	Object.assign( module.exports, require('./lib'))
	<% } %>

<% } else if( lib ){%>
module.exports = require('./lib');
<% } else {%>
module.exports = {};
<% } %>

<% if ( models ) { %>
// models
module.exports.models = require('./models')
<% } %>
