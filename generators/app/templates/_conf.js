/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Configuration options for <%= name %>
 * @module <%= name %>/conf
 * @author 
 * @since 0.1.0
 */
exports.<% name.toUpperCase() %>_FOO = 1
exports.<% name.toUpperCase() %>_THINGS = {
	BAR:{
		BAZ:2
	}
}
