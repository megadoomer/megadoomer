/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Loads configuration form different data stores before the server starts
 * @module megadoomer/generators/megadoomer/adpter
 * @author Eric Satterwhite 
 * @since 0.1.0
 */


var DataAdapter = module.exports =  function DataAdapter(){};

DataAdapter.prototype.prompt = function( questions, callback ) {
	callback( questions )

};

DataAdapter.prototype.diff = function(actual, expected) {
	return "diff not implemented"
};

DataAdapter.prototype.log = function(){
 // we don't log from here
}

DataAdapter.prototype.log.create = function(){};
