/*jshint node: true, smarttabs: true, esnext: true */
'use strict';
var util = require('util');
var path = require('path');
var debug = require('debug')('alice:generator');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var keef  = require('keef')


var AliceGenerator = yeoman.Base.extend({
  initializing: function () {
    this.pkg = require('../../package.json');
    this.sourceRoot(path.resolve(__dirname , 'templates' ) );
    this.destinationRoot( keef.get('PACKAGE_PATH') );
  },

  prompting: function () {
    var prompts = [{
      type: 'input',
      name: 'package',
      message: 'Name of package',
    }];

  },

  writing: {
    app: function () {
      var pkglocation = util.format(this.seeli_data.name );
      this.mkdir( pkglocation );
      
      ['conf','lib', 'test', 'commands', 'models', 'events', 'scripts', 'resources']
      .filter(function( dir ){
          return this.seeli_data[ dir ];
      }.bind( this ))
      .forEach(function( dir ){
          var directory = pkglocation + '/' + dir;
          debug('create directory %s ', directory);
          this.mkdir( directory );

          debug('generating %s README', dir );
          this.template('_README.md', `${directory}/README.md`, this.seeli_data);

          if( !(/test|scripts|commands/).test( dir ) ){
            debug('generating %s module', dir );
            this.template( `_${dir}.js`, `${directory}/index.js`, this.seeli_data);
          }

          if( dir == 'test'){
            debug('generating %s module', dir );
            this.template( `_${dir}.js`, `${directory}/${this.seeli_data.name}.spec.js`, this.seeli_data);
          }


      }.bind( this ));

      this.mkdir(`${pkglocation}/fixtures`);
      this.fs.copy(path.join(__dirname,'templates',('gitkeep')), pkglocation + "/fixtures/" + ".gitkeep" );

    },

    projectfiles: function () {
      var pkglocation = this.seeli_data.name;

      this.fs.copy(path.join(__dirname,'templates',('jshintrc')), pkglocation + '/' + '.jshintrc');

      debug('generating package.json ');
      this.template('_package.json', pkglocation + '/' + 'package.json', this.seeli_data );

      debug('generating main README ');
      this.template('_README.md', pkglocation + '/' + 'README.md', this.seeli_data );

      debug('generating package entry point ');
      this.template('_pkg.js', pkglocation + '/' + 'index.js', this.seeli_data );

      debug('generating gitkeep' );
      this.fs.copy(path.join(__dirname,'templates',('gitkeep')), pkglocation + "/" + ".gitkeep" );

      if( this.seeli_data.commands ){
        this.template('_commands.js', `${pkglocation}/commands/${this.seeli_data.name}.js` , this.seeli_data );
      }
    }
  },

  end: function () {
    // this.installDependencies();
  }
});

AliceGenerator.Adapter = require('./adapter');
module.exports = AliceGenerator;
