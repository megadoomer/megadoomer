var cli = require('seeli');
var yeoman = require("yeoman-generator")
var util = require('util')
var path = require('path');
var generator = require('../generators/app')
var env;

env = yeoman(null,{
    base: path.resolve(__dirname, '..', 'generators','app')
});

module.exports = new cli.Command({

    description:"Generate a new megadoomer project"
    ,flags:{
            'name':{
                type:String
                ,required:true
                ,description:`The name of your package ( ${cli.bold( 'payment' )} )`
            }

            ,'description':{
                type:String
                ,required:true
                ,description:"The general purpose of your  package"
            }

            , 'conf':{
                type:Boolean
              , default:true
              , description:"Generate a starter configuration module"
            }
            , 'lib':{
                type:Boolean
                , default:true
                , description:"Generate package libraries"
            }
            , 'commands':{
                type:Boolean
                , default:true
                , description:"Generate A starter Command"
            }
            , 'models':{
                type:Boolean
              , default:true
              , description:"Generate a starter models module"
            }
            , 'test':{
                type:Boolean
                , default:true
                ,description:"Generate a test spec"
            }
            , 'resources':{
                type:Boolean
                ,default: true
                , description:"Generate API resources directory"
            }
            , 'events':{
                type:Boolean
                , default:true
                ,description:"Generate package events"
            }
            , 'scripts':{
                type:Boolean
                , default:true
                , description:"Generate scripts directory"
            }
        }
  , run: function( cmd, data, done ){
        env.registerStub( generator, 'generator:megadoomer');
        var gen = env.create('generator:megadoomer', {options:data} )
        gen.seeli_data = data;
        gen.run( done.bind( null, null, '' ) )
    }
    
});
