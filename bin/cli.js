#!/usr/bin/env node
/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Command line Interface for alice. This module is here to load other management
 * commands. And that is about it.
 * @module megadoomer/bin/cli
 * @author Eric Satterwhite
 * @since 1.9.0
 * @requires seeli
 * @requires fs
 * @requires debug
 * @requires gaz/lang/clone
 */

 var cli = require( 'seeli' )
   , fs            = require('fs')                          // fs module
   , path          = require('path')                          // fs module
   , debug         = require('debug')( 'megadoomer:cli')
   , clone         = require('gaz/lang').clone
   , packagepath   = path.normalize( path.resolve(__dirname,'..','commands') )
   , jsregex       = /\.js$/
   , Loader        = require('gaz/fs/loader')
   , files
   ;

debug('current dir', __dirname);
debug('package path: %s', packagepath);

fs
	.readdirSync( packagepath )
	.forEach( function( file ){
		var searchpath = path.join( packagepath )
	    debug('search path: %s', searchpath);	
		if( fs.existsSync( searchpath ) && fs.statSync( searchpath ).isDirectory() ){
			fs.readdirSync(searchpath).forEach( function(module){
				if( jsregex.test( module ) ){
					var requirepath = path.join( searchpath, module )
					var cmd = require(requirepath)
					var name = ((cmd.options.name ? cmd.options.name : module)).replace(jsregex,'').toLowerCase().replace('-', '_');
					try{
						debug('loading %s', requirepath)
						debug('registering %s command', module )
						cli.use( name, cmd)
					} catch( e ){
						debug('unable to register module %s', module )
					}
				}
			} );
		}
	});
cli.set('color', 'blue')
cli.run()

